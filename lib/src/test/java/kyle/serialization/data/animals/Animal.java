package kyle.serialization.data.animals;

import java.util.Date;

import kyle.serialization.annotations.ParseClass;
import kyle.serialization.annotations.SelectSubclassByFieldValue;
import kyle.serialization.annotations.Subclasses;


@ParseClass ( regex =
        "^\\s*(?<name>\\w+)\\s*\\(\\s*(?<animalType>\\w+)\\s*\\)\\s*\n" +
        "Height\\s*:\\s*(?<height>\\d+(?:\\.\\d+)?).*\n" +
        "Weight\\s*:\\s*(?<weight>\\d+).*\n" +
        "Birth Date\\s*:\\s*(?<birthDate>\\S*)"
)
@Subclasses ( { Cat.class, Dog.class } )
public class Animal {

    protected String  name;
    protected Double  height;
    protected Integer weight;


    protected Date birthDate;

    @SelectSubclassByFieldValue
    protected String animalType;


    //@SelectSubclassWithMethod ( captureGroupName = "AnimalType" )
    public Class<? extends Animal> produceSubclass ( final String animalTypeString ) {
        return
            animalTypeString.equalsIgnoreCase("cat") ? Cat.class :
            animalTypeString.equalsIgnoreCase("dog") ? Dog.class :
            Animal.class;
    }


    public String getName () {
        return name;
    }


    public void setName ( final String name ) {
        this.name = name;
    }


    public Double getHeight () {
        return height;
    }


    public void setHeight ( final Double height ) {
        this.height = height;
    }


    public Integer getWeight () {
        return weight;
    }


    public void setWeight ( final Integer weight ) {
        this.weight = weight;
    }


    public Date getBirthDate () {
        return birthDate;
    }


    public void setBirthDate ( final Date birthDate ) {
        this.birthDate = birthDate;
    }


    public String getAnimalType () {
        return animalType;
    }


    public void setAnimalType ( final String animalType ) {
        this.animalType = animalType;
    }

}
