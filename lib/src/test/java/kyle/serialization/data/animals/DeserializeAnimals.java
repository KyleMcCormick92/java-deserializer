package kyle.serialization.data.animals;

import static kyle.serialization.data.utilities.file.FileReader.readFileToString;

import kyle.serialization.deserializer.Deserializer;


public class DeserializeAnimals {

    public static void main ( String[] args ) throws Throwable {
        final Animal animal = Deserializer.deserializeInto(
            readFileToString("lib/src/test/resources/fido.log"),
            Animal.class
        );
        System.out.println(animal.getAnimalType());
    }

}
