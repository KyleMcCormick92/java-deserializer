package kyle.serialization.data.utilities.file;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Paths.get;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.nio.file.Files;


public class FileReader {

    public static String readFileToString(final String filePath) throws IOException {
        return String.join("\n", Files.lines(get(filePath), UTF_8).collect(toList()));
    }

}
