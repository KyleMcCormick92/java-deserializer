package kyle.serialization.deserializer;

import static kyle.serialization.deserializer.ClassInstantiation.instantiate;
import static kyle.serialization.deserializer.subclassing.SubclassSelector.determineImmediateSubclass;
import static kyle.serialization.utils.ReflectionUtils.extractOrderedSuperclassList;

import java.util.ArrayList;
import java.util.List;


public class Deserializer {

    public static <T> T deserializeInto (
        final String   serialForm,
        final Class<T> clazz
    ) throws IllegalAccessException, InstantiationException {

        final List<FieldMapping> fieldMappings = parseSuperclassFields(serialForm, clazz);

        final Class<? extends T> resultantClass = determineClass(serialForm, clazz, fieldMappings);

        return instantiate(resultantClass).with(fieldMappings).andExecuteDelegates();
    }


    private static <T> List<FieldMapping> parseSuperclassFields(final String serialForm, final Class<T> clazz){
        final List<FieldMapping> resultingFieldMappings = new ArrayList<>();
        final List<Class<? super T>> superclasses = extractOrderedSuperclassList(clazz); // TODO: write this
        // TODO: requires implemented interfaces?

        for ( Class<? super T> superclass : superclasses ) {
            parseDirectFields(serialForm, superclass, resultingFieldMappings);
        }

        return resultingFieldMappings;
    }


    private static <T> Class<? extends T> determineClass (
        final String             serialForm,
        final Class<T>           clazz,
        final List<FieldMapping> fieldMappings
    ) throws IllegalAccessException, InstantiationException {

        parseDirectFields(serialForm, clazz, fieldMappings);

        final Class<? extends T> sub = determineImmediateSubclass(serialForm, clazz, fieldMappings);

        return sub == clazz
                ? clazz
                : determineClass(serialForm, sub, fieldMappings); // TODO: serial form passed down might change
    }


    private static <T> void parseDirectFields (
        final String             serialForm,
        final Class<T>           clazz,
        final List<FieldMapping> fieldMappings
    ){
        // TODO: parse fields

        // TODO: should 'parseDirectFields' execute delegate methods w/ 'final' modifier?
        // No? because the object won't necessarily be created yet?
    }


}
