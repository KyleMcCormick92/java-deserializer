package kyle.serialization.deserializer;


import static kyle.serialization.utils.ReflectionUtils.hasDefaultConstructor;
import static kyle.serialization.utils.ReflectionUtils.invokeDefaultConstructor;

import java.util.List;


class ClassInstantiation <T> {

    private Class<T> clazz;
    private T        instantiation;


    static <T> ClassInstantiation<T> instantiate (
        final Class<T> clazz
    )
        throws InstantiationException, IllegalAccessException
    {
        final ClassInstantiation<T> result = new ClassInstantiation<>(clazz);
        if ( hasDefaultConstructor(clazz) ) {
            result.instantiation = invokeDefaultConstructor(clazz);
        }
        return result;
    }


    private ClassInstantiation ( final Class<T> clazz ) {
        this.clazz = clazz;
    }


    ClassInstantiation<T> with ( final List<FieldMapping> fieldMappings ) {
        // TODO: map fields
        return this;
    }


    T andExecuteDelegates () {
        // TODO: execute delegates
        return instantiation;
    }

}
