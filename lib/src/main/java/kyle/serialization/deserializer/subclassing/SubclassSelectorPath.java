package kyle.serialization.deserializer.subclassing;

import java.util.List;

import kyle.serialization.deserializer.FieldMapping;


interface SubclassSelectorPath <T> {

    Class<? extends T> determineSubclass (
        String             serialForm,
        Class<T>           clazz,
        List<FieldMapping> fieldMappings
    )
      throws
        IllegalAccessException,
        InstantiationException;

}
