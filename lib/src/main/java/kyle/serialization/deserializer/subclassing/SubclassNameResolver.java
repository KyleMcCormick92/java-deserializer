package kyle.serialization.deserializer.subclassing;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import kyle.serialization.annotations.Subclasses;


public final class SubclassNameResolver {

    private static final Pattern SPECIAL_CHARACTER_FILTER_REGEX = Pattern.compile("\\W+");

    private static final Map<Class<?>, Map<String, Class<?>>> classToSubclassesMap = new HashMap<>();


    private SubclassNameResolver () {}


    @SuppressWarnings ( "unchecked" )
    public static <T> Class<? extends T> resolveSubclassFromString (
        final String inputSubclassName,
        final Class<T> clazz,
        boolean ignoreCase,
        boolean ignoreSpecialCharacters
    ) {
        final Map<String, Class<?>> subclassNameMap = classToSubclassesMap.computeIfAbsent(
            clazz,
            clz -> generateSubclassMap(clz, ignoreCase, ignoreSpecialCharacters)
        );
        final String adjustedInput = adjustString(inputSubclassName, ignoreCase, ignoreSpecialCharacters);

        final Class<? extends T> subclass = (Class<? extends T>) subclassNameMap.get(adjustedInput);
        return subclass != null ? subclass : clazz;
    }


    public static String adjustString ( final String input, boolean ignoreCase, boolean ignoreSpecialCharacters ) {
        final String properlyCasedString = ignoreCase
                                           ? input.toLowerCase()
                                           : input;
        return ignoreSpecialCharacters
               ? SPECIAL_CHARACTER_FILTER_REGEX
                   .matcher(properlyCasedString)
                   .replaceAll("")
               : properlyCasedString;
    }


    private static Map<String, Class<?>> generateSubclassMap (
        final Class<?> clazz,
        boolean ignoreCase,
        boolean ignoreSpecialCharacters
    ) {
        final Map<String, Class<?>> result = new HashMap<>();
        if ( clazz.isAnnotationPresent(Subclasses.class) ) {
            for ( Class<?> subclass : clazz.getAnnotation(Subclasses.class).value() ) {
                if ( !subclass.isAssignableFrom(clazz) ) { // TODO is this backward?
                    // TODO: throw exception
                }
                final String processedSubclassName = adjustString(
                    subclass.getSimpleName(),
                    ignoreCase,
                    ignoreSpecialCharacters
                );
                result.put(processedSubclassName, subclass);
            }
        } else {
            // TODO: throw exception
        }
        return result;
    }

}
