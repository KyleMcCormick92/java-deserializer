package kyle.serialization.deserializer.subclassing;

import java.util.List;

import kyle.serialization.annotations.SelectSubclassWithCaptureGroup;
import kyle.serialization.deserializer.FieldMapping;


final class CaptureGroupSubclassSelector <T> implements SubclassSelectorPath<T> {


    private String  captureGroupName;

    private boolean ignoreCaseInClassName;

    private boolean ignoreSpecialCharactersInClassName;


    public CaptureGroupSubclassSelector ( final Class<T> clazz ) {
        final SelectSubclassWithCaptureGroup annotation = clazz.getAnnotation(SelectSubclassWithCaptureGroup.class);
        this.captureGroupName                   = annotation.captureGroupName();
        this.ignoreCaseInClassName              = annotation.ignoreCaseInClassName();
        this.ignoreSpecialCharactersInClassName = annotation.ignoreSpecialCharactersInClassName();
    }


    @Override
    public Class<? extends T> determineSubclass (
        final String serialForm,
        final Class<T> clazz,
        final List<FieldMapping> fieldMappings
    ) throws IllegalAccessException, InstantiationException {
        // TODO
        return clazz;
    }

}
