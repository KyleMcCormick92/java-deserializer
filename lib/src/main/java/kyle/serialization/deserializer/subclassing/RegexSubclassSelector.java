package kyle.serialization.deserializer.subclassing;

import java.util.List;

import kyle.serialization.annotations.SelectSubclassByRegex;
import kyle.serialization.deserializer.FieldMapping;


final class RegexSubclassSelector <T> implements SubclassSelectorPath<T> {

    private String  regex;

    private String  captureGroupName;

    private boolean ignoreCaseInRegex;

    private boolean ignoreCaseInClassName;

    private boolean ignoreSpecialCharactersInClassName;


    public RegexSubclassSelector ( final Class<T> clazz ) {
        final SelectSubclassByRegex annotation = clazz.getAnnotation(SelectSubclassByRegex.class);
        this.regex                              = annotation.regex();
        this.captureGroupName                   = annotation.captureGroupName();
        this.ignoreCaseInRegex                  = annotation.ignoreCaseInRegex();
        this.ignoreCaseInClassName              = annotation.ignoreCaseInClassName();
        this.ignoreSpecialCharactersInClassName = annotation.ignoreSpecialCharactersInClassName();
    }

    @Override
    public Class<? extends T> determineSubclass (
        final String serialForm,
        final Class<T> clazz,
        final List<FieldMapping> fieldMappings
    ) throws IllegalAccessException, InstantiationException {
        // TODO
        return clazz;
    }

}
