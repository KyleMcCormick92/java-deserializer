package kyle.serialization.deserializer.subclassing;

import java.util.List;

import kyle.serialization.annotations.SelectSubclassWithMethod;
import kyle.serialization.deserializer.FieldMapping;


final class MethodSubclassSelector <T> implements SubclassSelectorPath<T> {

    private String  regex;

    private String  captureGroupName;

    private boolean ignoreCaseInRegex;

    private boolean ignoreCaseInClassName;

    private boolean ignoreSpecialCharactersInClassName;

    private boolean selectSubclassBeforeParsingSuperclass;


    public MethodSubclassSelector ( final Class<T> clazz ) {
        final SelectSubclassWithMethod annotation = clazz.getAnnotation(SelectSubclassWithMethod.class);
        this.regex                                 = annotation.regex();
        this.captureGroupName                      = annotation.captureGroupName();
        this.ignoreCaseInRegex                     = annotation.ignoreCaseInRegex();
        this.ignoreCaseInClassName                 = annotation.ignoreCaseInClassName();
        this.ignoreSpecialCharactersInClassName    = annotation.ignoreSpecialCharactersInClassName();
        this.selectSubclassBeforeParsingSuperclass = annotation.selectSubclassBeforeParsingSuperclass();
    }


    @Override
    public Class<? extends T> determineSubclass (
        final String serialForm,
        final Class<T> clazz,
        final List<FieldMapping> fieldMappings
    ) throws IllegalAccessException, InstantiationException {
        // TODO
        return clazz;
    }

}
