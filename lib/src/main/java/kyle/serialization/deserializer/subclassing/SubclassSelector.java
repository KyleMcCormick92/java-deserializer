package kyle.serialization.deserializer.subclassing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kyle.serialization.deserializer.FieldMapping;


public final class SubclassSelector {

    private static final Map<Class<?>, SubclassSelectorPath> subclassSelectors = new HashMap<>();


    @SuppressWarnings ( "unchecked" )
    public static <T> Class<? extends T> determineImmediateSubclass (
        final String             serialForm,
        final Class<T>           clazz,
        final List<FieldMapping> fieldMappings
    ) throws InstantiationException, IllegalAccessException {
        final SubclassSelectorPath<T> selectorPath = (SubclassSelectorPath<T>) subclassSelectors.computeIfAbsent(
            clazz,
            SubclassSelectorPathFactory::getSubclassSelectorPath
        );
        return selectorPath.determineSubclass(serialForm, clazz, fieldMappings);
    }


    /*
    @Override
    public Class<? extends T> determineSubclass (
        final String             serialForm,
        final Class<T>           clazz,
        final List<FieldMapping> fieldMappings
    ) throws IllegalAccessException, InstantiationException {
        if ( ReflectionUtils.isNotFinal(clazz) ) {
            if ( hasAnnotation(clazz, Subclasses.class) ) {
                if ( hasAnnotation(clazz, SelectSubclassByFieldValue.class) ) {
                    // TODO: return subclass selected by field value
                }
                if ( hasAnnotation(clazz, SelectSubclassWithCaptureGroup.class) ) {
                    // TODO: return subclass selected by capture group
                }
                if ( hasAnnotation(clazz, SelectSubclassByRegex.class) ) {
                    // TODO: return subclass selected by new regex
                }
                if ( hasStaticMethodWithAnnotation(clazz, SelectSubclassWithMethod.class) ) {
                    // TODO: return subclass selected by static method
                }
                if ( hasNonStaticMethodWithAnnotation(clazz, SelectSubclassWithMethod.class) ) {
                    // TODO: return subclass selected by member method
                }
                // TODO: log warning? Throw error?
            } else {
                if ( hasFieldWithAnnotationAndOfType(clazz, SelectSubclassByFieldValue.class, Class.class) ) {

                }
                if ( hasMethodWithAnnotationAndReturnType(clazz, SelectSubclassWithMethod.class, Class.class) ) {

                }
            }
            if ( isAbstract(clazz) ) {
                if ( hasDefaultSubclass(clazz) ) {
                    // TODO: return default subclass
                }
                // TODO: Throw error?
            }

        }
        return clazz;
    }
    //*/

}
