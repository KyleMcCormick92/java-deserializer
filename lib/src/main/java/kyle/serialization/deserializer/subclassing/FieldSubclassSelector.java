package kyle.serialization.deserializer.subclassing;

import static kyle.serialization.utils.ReflectionUtils.extractFieldsWithAnnotation;
import static kyle.serialization.utils.ReflectionUtils.hasAnnotation;
import static kyle.serialization.utils.ReflectionUtils.hasFieldWithAnnotation;
import static kyle.serialization.utils.ReflectionUtils.hasMethodWithAnnotation;

import java.lang.reflect.Field;
import java.util.List;

import kyle.serialization.annotations.SelectSubclassByFieldValue;
import kyle.serialization.annotations.SelectSubclassByRegex;
import kyle.serialization.annotations.SelectSubclassWithCaptureGroup;
import kyle.serialization.annotations.SelectSubclassWithMethod;
import kyle.serialization.annotations.Subclasses;
import kyle.serialization.deserializer.FieldMapping;
import kyle.serialization.utils.ReflectionUtils;


final class FieldSubclassSelector <T> implements SubclassSelectorPath<T> {

    private boolean ignoreCaseInClassName;

    private boolean ignoreSpecialCharactersInClassName;

    private Field fieldUsedForSubclassSelection;

    private Class<?> fieldType;


    @SuppressWarnings ( "unchecked" )
    public FieldSubclassSelector ( final Class<T> clazz ) throws Throwable {
        final List<Field> fieldsForSubclassSelection = extractFieldsWithAnnotation(
            clazz,
            SelectSubclassByFieldValue.class
        );
        if ( fieldsForSubclassSelection.size() != 1 ) {
            // TODO throw exception
        }
        this.fieldUsedForSubclassSelection = fieldsForSubclassSelection.get(0);

        final SelectSubclassByFieldValue annotation =
            fieldUsedForSubclassSelection.getAnnotation(SelectSubclassByFieldValue.class);

        this.ignoreCaseInClassName = annotation.ignoreCaseInClassName();
        this.ignoreSpecialCharactersInClassName = annotation.ignoreSpecialCharactersInClassName();

        this.fieldType = fieldUsedForSubclassSelection.getType();
        fieldUsedForSubclassSelection.setAccessible(true); // TODO necessary?

    }


    @Override
    public Class<? extends T> determineSubclass (
        final String             serialForm,
        final Class<T>           clazz,
        final List<FieldMapping> fieldMappings
    ) throws IllegalAccessException, InstantiationException {
        return clazz;
    }

}
