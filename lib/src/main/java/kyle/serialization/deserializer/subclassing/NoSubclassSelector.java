package kyle.serialization.deserializer.subclassing;

import java.util.List;

import kyle.serialization.deserializer.FieldMapping;


final class NoSubclassSelector <T> implements SubclassSelectorPath<T> {

    @Override
    public Class<? extends T> determineSubclass (
        final String serialForm,
        final Class<T> clazz,
        final List<FieldMapping> fieldMappings
    ) throws IllegalAccessException, InstantiationException {
        // TODO
        return clazz;
    }

}
