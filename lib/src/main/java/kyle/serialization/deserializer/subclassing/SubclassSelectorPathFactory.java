package kyle.serialization.deserializer.subclassing;

import static kyle.serialization.utils.ReflectionUtils.hasAnnotation;
import static kyle.serialization.utils.ReflectionUtils.hasFieldWithAnnotation;
import static kyle.serialization.utils.ReflectionUtils.hasMethodWithAnnotation;

import kyle.serialization.annotations.SelectSubclassByFieldValue;
import kyle.serialization.annotations.SelectSubclassByRegex;
import kyle.serialization.annotations.SelectSubclassWithCaptureGroup;
import kyle.serialization.annotations.SelectSubclassWithMethod;


final class SubclassSelectorPathFactory {

    static <T> SubclassSelectorPath<T> getSubclassSelectorPath ( final Class<T> clazz ) {

        try {
            if ( hasMethodWithAnnotation(clazz, SelectSubclassWithMethod.class) ) {
                return new MethodSubclassSelector<>(clazz);
            }
            if ( hasFieldWithAnnotation(clazz, SelectSubclassByFieldValue.class) ) {
                return new FieldSubclassSelector<>(clazz);
            }
            if ( hasAnnotation(clazz, SelectSubclassWithCaptureGroup.class) ) {
                return new CaptureGroupSubclassSelector<>(clazz);
                // TODO: different selector class for when we want to select using a new regex?
            }
            if ( hasAnnotation(clazz, SelectSubclassByRegex.class) ) {
                return new RegexSubclassSelector<>(clazz);
                // TODO: different selector class for when we want to select using a new regex?

            }
            // TODO: Throw exception?
            // TODO: or attempt to parse each subclass?
        } catch(Throwable t){
            t.getMessage();
        }

        return new NoSubclassSelector<>();
    }

}
