package kyle.serialization.deserializer;


import java.util.HashMap;
import java.util.Map;


public class FieldMapping {

    private Class<?>            declaringClass;

    private Map<String, Object> fieldMappingsForDeclaredClass;

    private FieldMapping        parent;

    private FieldMapping        child;


    FieldMapping(final Class<?> declaringClass){
        this.declaringClass                = declaringClass;
        this.fieldMappingsForDeclaredClass = new HashMap<>();
    }

}
