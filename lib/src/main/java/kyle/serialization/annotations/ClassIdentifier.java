package kyle.serialization.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention ( RetentionPolicy.RUNTIME )
@Target ( ElementType.TYPE )
@Inherited
public @interface ClassIdentifier { // Used for subclass identification

    String value(); // Regex or literal value

    boolean ignoreCase() default true;

    boolean ignoreSpecialCharactersIfLiteral() default true;

    boolean isRegex() default false;

}
