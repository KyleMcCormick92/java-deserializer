package kyle.serialization.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention ( RetentionPolicy.RUNTIME )
@Target ( ElementType.FIELD )
public @interface SelectSubclassByFieldValue {

    boolean ignoreCaseInClassName() default true;

    boolean ignoreSpecialCharactersInClassName() default true;

}
