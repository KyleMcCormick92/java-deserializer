package kyle.serialization.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * METHOD STRUCTURE
 *
 * The methods which this annotation tags are known as "subclass selection methods".
 * Subclass selection methods are expected to have a signature of the following form:
 *
 *     static [Class\<\?\>|String] methodName(String input [, Map\<String, Object\> discoveredFields])
 *     // TODO: redefine all definitions below to handle the optional parameter discoveredFields
 *
 *
 * SELECTION TECHNIQUES
 *
 * There are three evaluation modes for this subclass selection technique:
 * TODO: what about when the method is static? This would allow us to not instantiate an object and have to copy any
 *       discovered fields (or pre-discovered fields from a superclass)
 *
 * 1. IF     The @regex and @captureGroupName are both non-empty
 *    THEN   The entire serial form is evaluated against the @regex.
 *           The capture group with same name as @captureGroupName is passed into the subclass selection method.
 *           If @selectSubclassBeforeParsingSuperclass is true, then the subclass is selected before evaluating any
 *           @ParseClass or @ExtractField annotations.
 *
 * 2. IF     The @regex is empty and the @captureGroupName is non-empty
 *    THEN   After the regex from the @ParseClass method is evaluated, the capture group with same name as
 *           @captureGroupName is passed into the subclass selection method.
 *           In this evaluation mode, the value of @selectSubclassBeforeParsingSuperclass is ignored.
 *
 * 3. IF     The @regex and @captureGroupName are both empty
 *    THEN   The entire serial form is passed into the subclass selection method.
 *           If @selectSubclassBeforeParsingSuperclass is true, then the subclass is selected before evaluating any
 *           @ParseClass or @ExtractField annotations.
 *
 *
 * RETURN TYPES
 *
 * Class<?>  If the subclass selection method has a return type of Class<?>, then the return value is directly used
 *           to determine the subclass.
 *           With a Class<?> return type, the values of @ignoreCaseInClassName and
 *           @ignoreSpecialCharactersInClassName are both ignored.
 *
 * String    If the subclass selection method has a return type of java.lang.String, then the return value is mapped
 *           using the String-to-Class mapping described in SubclassNameResolver.java
 **/

@Retention ( RetentionPolicy.RUNTIME )
@Target ( ElementType.METHOD )
public @interface SelectSubclassWithMethod {

    String regex() default "";

    String captureGroupName() default "";

    boolean ignoreCaseInRegex() default false;

    boolean ignoreCaseInClassName() default true;

    boolean ignoreSpecialCharactersInClassName() default true;

    boolean selectSubclassBeforeParsingSuperclass() default true;

    // TODO: Should add a "has side effect" or "always execute" field, for handling cases in which the original class
    //       for which deserialization was requested has an ancestor class declaring a method with this annotation?
    //
    //       For example, if `Deserialize.deserializeInto(serialForm, Cat.class)` was called, but Animal.class had a
    //       method annotated `@SelectSubclassWithMethod` which had a side-effect of populating a field?

}
