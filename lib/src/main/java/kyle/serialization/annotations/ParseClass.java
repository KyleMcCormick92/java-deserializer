package kyle.serialization.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention ( RetentionPolicy.RUNTIME )
@Target ( ElementType.TYPE )
@Inherited
public @interface ParseClass {

    String regex(); // the parsing regex

    boolean ignoreCaseInRegex() default false;

    boolean ignoreCaseInFieldNames() default false;

    boolean shouldStillParseSuperclass() default true;

    String groupToPassToSubclass() default ""; // If empty, entire regex is sent to the subclass

}
