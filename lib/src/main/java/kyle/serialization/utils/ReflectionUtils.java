package kyle.serialization.utils;

import static java.util.Arrays.stream;
import static java.util.Collections.reverse;
import static java.util.stream.Collectors.toList;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import kyle.serialization.annotations.SelectSubclassByRegex;
import kyle.serialization.annotations.SelectSubclassWithCaptureGroup;
import kyle.serialization.annotations.Subclasses;


public class ReflectionUtils {


    public static boolean hasSubclassList ( final Class<?> clazz ) {
        return clazz.isAnnotationPresent(Subclasses.class);
    }


    public static boolean hasSubclassSelectorRegex ( final Class<?> clazz ) {
        return clazz.isAnnotationPresent(SelectSubclassByRegex.class);
    }


    public static boolean hasSubclassSelectorCaptureGroup ( final Class<?> clazz ) {
        return clazz.isAnnotationPresent(SelectSubclassWithCaptureGroup.class);
    }


    public static boolean hasAnnotation (
        final Class<?> clazz,
        final Class<? extends Annotation> annotationClass
    ) {
        return clazz.isAnnotationPresent(annotationClass);
    }


    public static boolean hasMethodWithAnnotation (
        final Class<?> clazz,
        final Class<? extends Annotation> annotationClass
    ) {
        return stream(clazz.getMethods())
            .anyMatch(m -> m.isAnnotationPresent(annotationClass));
    }


    public static boolean hasFieldWithAnnotation (
        final Class<?> clazz,
        final Class<? extends Annotation> annotationClass
    ) {
        return stream(clazz.getDeclaredFields())
            .anyMatch(m -> m.isAnnotationPresent(annotationClass));
    }


    public static List<Field> extractFieldsWithAnnotation (
        final Class<?> clazz,
        final Class<? extends Annotation> annotationClass
    ) {
        return stream(clazz.getDeclaredFields())
            .filter(m -> m.isAnnotationPresent(annotationClass))
            .collect(toList());
    }


    public static boolean hasDefaultConstructor ( final Class<?> clazz ) {
        try {
            return clazz.getDeclaredConstructor() != null; // TODO: is correct?
        } catch ( NoSuchMethodException e ) { // TODO: rewrite without using exception throwing/catching?
            return false;
        }
    }


    public static <T> T invokeDefaultConstructor (
        final Class<T> clazz
    ) throws IllegalAccessException, InstantiationException {
        return clazz.newInstance();
    }


    public static <T> List< Class<? super T> > extractOrderedSuperclassList ( final Class<T> clazz ) {
        final List< Class<? super T> > result = new ArrayList<>();

        Class<? super T> tmpClass = clazz;
        while (tmpClass.getSuperclass() != Object.class) {
            tmpClass = tmpClass.getSuperclass();
            result.add(tmpClass);
        }
        reverse(result);
        return result;
    }


    /*
    public static Supplier getter( Object obj, Class<?> cls, Field f) throws Throwable {
        boolean isStatic = Modifier.isStatic(f.getModifiers());
        MethodType sSig = MethodType.methodType(f.getType());
        Class<?> dCls = Supplier.class;
        MethodType dSig = MethodType.methodType(Object.class);
        String dMethod = "get";
        MethodType dType = isStatic? MethodType.methodType(dCls) : MethodType.methodType(dCls, cls);
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodHandle factory = LambdaMetafactory.metafactory(lookup, dMethod, dType, dSig, lookup.unreflectGetter(f),
         sSig).getTarget();
        factory = !isStatic && obj!=null? factory.bindTo(obj) : factory;
        return (Supplier)factory.invoke();
    }
    */


    /*
    public static Function createGetter(
        final MethodHandles.Lookup lookup,
        final MethodHandle getter
    ) throws Exception {
        final CallSite site = LambdaMetafactory.metafactory(
            lookup, "apply",
            MethodType.methodType(Function.class),
            MethodType.methodType(Object.class, Object.class), //signature of method Function.apply after type erasure
            getter,
            getter.type()); //actual signature of getter
        try {
            return (Function) site.getTarget().invokeExact();
        } catch (final Exception e) {
            throw e;
        } catch (final Throwable e) {
            throw new Error(e);
        }
    }
    */

}
