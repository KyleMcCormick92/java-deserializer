package kyle.serialization.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateUtils {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy");


    public static Date formatDate(final String dateString) throws ParseException {
        return DATE_FORMAT.parse(dateString);
    }

}
